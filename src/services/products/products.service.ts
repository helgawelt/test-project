import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  products: any[];
  productsUploaded = new Subject();

  constructor() {
    this.getData();
  }

  /**
   * Uploading test data
   */
  getData() {
    fetch('assets/data/data.json').then(res=>res.json()).then(data=>{
      this.products = data;
      this.productsUploaded.next(this.products);
    });
  }
}
