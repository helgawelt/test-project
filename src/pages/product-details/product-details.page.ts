import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { from } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductsService } from '../../services/products/products.service'

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.page.html',
  styleUrls: ['./product-details.page.scss'],
})
export class ProductDetailsPage implements OnInit {
  product;
  calcData = {
    amount: 0,
    quantity: 0
  };

  constructor(private route: ActivatedRoute,
              private router: Router,
              private productsService: ProductsService) { }

  ngOnInit() {
    const sku = this.route.snapshot.paramMap.get('sku');
    this.getProductItem(sku);
  }

  /**
   * Find product by sku
   * @param {number} sku - unique id.
   */
  getProductItem(sku) {
    from(this.productsService.products)
    .pipe(first((item: any) => Number(item.sku) === Number(sku * 1)))
    .subscribe(data => {
      this.product = data;
    });
  }

  /**
   * Calculation of the dependence of the quantity on the price
   * @param {number} type - type of changed input (1 - amount, 2 - quantity)
   */
  calc(type) {
    if (type === 1) {
      this.calcData.quantity = Number((this.calcData.amount / this.product.price).toFixed(1));
    } else {
      this.calcData.amount = Number((this.calcData.quantity * this.product.price).toFixed(1));
    }
  }

  /**
   * Calculation of the dependence of the quantity on the price
   * @param {number} type - type of changed input (1 - amount, 2 - quantity)
   * @param {number} value - value of inpyt
   */
  setData(type, value) {
    if (type === 1) {
      this.calcData.amount = value;
      this.calc(type);
    } else {
      this.calcData.quantity = value;
      this.calc(type);
    }
  }
}
