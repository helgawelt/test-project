import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { of, Subscription } from 'rxjs';
import { Storage } from '@ionic/storage-angular';
import { ProductsService } from '../../services/products/products.service'

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.page.html',
  styleUrls: ['./products-list.page.scss'],
})
export class ProductsListPage implements OnInit {
  filteredProducts: any[];
  searchLine = '';
  productsUploaded: Subscription;

  constructor(private storage: Storage,
              private productsService: ProductsService) {
    this.initPage();
  }

  ngOnInit() {
    this.productsUploaded = this.productsService.productsUploaded.subscribe(data => {
      this.initPage();
    });
  }

  ionViewDidLeave() {
    this.productsUploaded.unsubscribe();
  }

  initPage() {
    if (this.productsService.products) {
      this.filteredProducts = this.productsService.products;

      // Call Ionic storage initialization
      this.init();
    }
  }

  /**
   * Filter data by 'name' field
   */
  searchByName() {
    this.set('name', this.searchLine);

    of(this.productsService.products)
    .pipe(
      map((items: any) => items.filter((item: any) => item.name.toLowerCase().indexOf(this.searchLine.toLowerCase()) !== -1)))
    .subscribe(data => {
      this.filteredProducts = data;
    });
  }

  /**
   * Ionic storage initialization, saving string from storage
   */
  async init() {
    await this.storage.create();

    this.get('name').then((value) => {
      if (value) {
        this.searchLine = value;
        this.searchByName();
      }
    });
  }

  /**
   * Setter for Ionic storage
   * @param {string} settingName - key.
   * @param {string} value - new key's value.
   */
  public set(settingName, value){
    return this.storage.set(`setting:${ settingName }`,value);
  }

  /**
   * Getter for Ionic storage
   * @param {string} settingName - key.
   */
  public async get(settingName){
    return await this.storage.get(`setting:${ settingName }`);
  }
}
