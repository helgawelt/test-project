import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'products-list',
    loadChildren: () => import('../pages/products-list/products-list.module').then( m => m.ProductsListPageModule)
  },
  {
    path: 'product-details/:sku',
    loadChildren: () => import('../pages/product-details/product-details.module').then( m => m.ProductDetailsPageModule)
  },
  {
    path: '',
    redirectTo: 'products-list',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
